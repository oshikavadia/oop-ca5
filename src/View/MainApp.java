package View;

import Utility.MenuOptions;
import Model.Movie;
import Controller.RecordManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

/*
 * @author oshi
 */
public class MainApp {

    private MenuOptions mainMenu, searchMenu, sortMenu;

    public final int RECORD_SIZE = 325;
//    public ArrayList<Movie> movieList = new ArrayList<Movie>();
    public RecordManager rm;
    public RandomAccessFile raf;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        MainApp mainApp = new MainApp();
        mainApp.start();
    }

    public void start() throws FileNotFoundException, IOException {

//        String filePath = "//home//oshi//OOPCA//";
        System.out.println("\t\t\t*********Movie Database*********");
        String filePath = Utility.ScannerUtility.getString("Please enter a filepath(Dont forget to add a slash at the end): ");
        File f = new File(filePath);
        while (!f.isDirectory()) {
            filePath = Utility.ScannerUtility.getString("Please enter a valid filepath!(Dont forget to add a slash at the end): ");
        }
        rm = new RecordManager(filePath);
        initialiseMenus();
        showMainMenu();
    }

    private void showMainMenu() throws IOException {
        int choice = 0;

        do {
            choice = this.mainMenu.showMenuGetChoice("Enter your choice: ");

            switch (choice) {
                case 1:
                    rm.createDatabase();
                    break;
                case 2:
                    String fileName = Utility.ScannerUtility.getString("Enter Database name :");

                    raf = rm.openDatabase(fileName);
                    break;
                case 3:
                    fileName = Utility.ScannerUtility.getString("Enter Database name :");
                    boolean confirmation = Utility.ScannerUtility.getBoolean("Do you really want to delete this database? : ");
                    rm.deleteDatabase(confirmation, fileName);
                    break;
                case 4:
                    if (checkifNull(raf)) {
                        System.out.println("Please Open a new Database first!");
                        break;
                    }
                    String name = Utility.ScannerUtility.getString("Enter Movie name (Under 45 characters): ");
                    while (name.length() > 45) {
                        name = Utility.StringUtility.pad(Utility.ScannerUtility.getString("Please enter a name under 45 characters "), 45, "*");
                    }
                    String desc = Utility.ScannerUtility.getString("Enter Movie Desc (Under 255 Characters):");
                    while (desc.length() > 255) {
                        desc = Utility.StringUtility.pad(Utility.ScannerUtility.getString("Please enter a name under 255 characters "), 255, "*");
                    }
                    double budget = Utility.ScannerUtility.getDouble("Enter movie budget: ");
                    double ticketPrice = Utility.ScannerUtility.getDouble("Enter Ticket price: ");
                    boolean is3d = Utility.ScannerUtility.getBoolean("Is the movie 3d (true/false): ");
                    int runtime = Utility.ScannerUtility.getInt("Enter runtime :");

                    rm.addRecord(raf, name, desc, budget, ticketPrice, is3d, runtime);
                    break;
                case 5:
                    if (checkifNull(raf)) {
                        System.out.println("Please Open a new Database first!");
                        break;
                    }
                    int index = Utility.ScannerUtility.getInt("Enter the index number(zero index) :");
                    int editChoice = Utility.ScannerUtility.getInt("What do you want to edit?\n1)Name\n2)Desc\n3)Budget\n4)Ticket Price\n5)Runtime\n6)is3d\n");
                    rm.edit(raf, index, editChoice);
                    break;
                case 6:
                    showSearchMenu();
                    break;
                case 7:
                    if (checkifNull(raf)) {
                        System.out.println("Please Open a new Database first!");
                        break;
                    }
                    int deleteIndex = Utility.ScannerUtility.getInt("Enter the record number to delete(0 index) :");
                    confirmation = Utility.ScannerUtility.getBoolean("Do you really want to delete this record?(true/false) : ");
                    rm.deleteRecord(raf, confirmation, deleteIndex);
                    break;
                case 8:
                    if (checkifNull(raf)) {
                        System.out.println("Please Open a new Database first!");
                        break;
                    }
                    confirmation = Utility.ScannerUtility.getBoolean("Do you really want to delete all records? : ");
                    rm.deleteAll(raf, confirmation);
                    break;
                case 9:
                    if (checkifNull(raf)) {
                        System.out.println("Please Open a new Database first!");
                        break;
                    }
                    rm.displayAll();
                    break;
                case 10:
                    if (checkifNull(raf)) {
                        System.out.println("Please Open a new Database first!");
                        break;
                    }
                    System.out.println("Number of records in the database : " + rm.showCount());
                default:
                    break;
            }

        } while (choice != this.mainMenu.getIndexOfExitOption());

        System.out.println("Goodbye...");

    }

    /**
     * *************************************** Search Related Methods
     * ****************************************
     */
    private void showSearchMenu() {
        int choice = 0;

        do {
            choice = this.searchMenu.showMenuGetChoice("Enter your choice: ");

            switch (choice) {
                case 1:
                    String searchCriteria = Utility.ScannerUtility.getString("Enter Search Value: ");
                    rm.searchByName(searchCriteria);
                    break;
                case 2:
                    searchCriteria = Utility.ScannerUtility.getString("Enter Search Value: ");
                    rm.searchByDesc(searchCriteria);
                    break;
                case 3:
                    double searchCriteriaDouble = Utility.ScannerUtility.getDouble("Enter Search Value: ");
                    rm.searchBybudget(searchCriteriaDouble);
                    break;
                case 4:
                    searchCriteriaDouble = Utility.ScannerUtility.getDouble("Enter Search Value: ");
                    rm.searchByTicketPrice(searchCriteriaDouble);
                case 5:
                    int searchCriteriaInt = Utility.ScannerUtility.getInt("Enter Search Value: ");
                    rm.searchByRunTime(searchCriteriaInt);
                    break;
                case 6:
                    boolean searchCriteriaBool = Utility.ScannerUtility.getBoolean("Enter Search Value: ");
                    rm.searchBy3D(searchCriteriaBool);
                default:
                    break;
            }

        } while (choice != this.searchMenu.getIndexOfExitOption());

    }

    /**
     * *************************************** Initialisation Methods
     * ****************************************
     */
    private void initialiseMenus() {
        //main menu
        this.mainMenu = new MenuOptions("Main Menu", 11);
        this.mainMenu.add("Create Databse");
        this.mainMenu.add("Open Database");
        this.mainMenu.add("Delete DataBase");
        this.mainMenu.add("Add");
        this.mainMenu.add("Edit");
        this.mainMenu.add("Search");
        this.mainMenu.add("Delete record");
        this.mainMenu.add("Delete all records");
        this.mainMenu.add("ShowAll");
        this.mainMenu.add("Count");
        this.mainMenu.add("Exit");
        //search menu
        this.searchMenu = new MenuOptions("Search Menu", 7);
        this.searchMenu.add("Search By Name");
        this.searchMenu.add("Search By Desc");
        this.searchMenu.add("Search By Budget");
        this.searchMenu.add("Search By Ticket Price");
        this.searchMenu.add("Search By Run time");
        this.searchMenu.add("Search By 3D");
        this.searchMenu.add("Back to Main Menu");
    }

    public boolean checkifNull(RandomAccessFile raf) {
        return raf == null;
    }
}
