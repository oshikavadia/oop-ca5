/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MovieTest;

import Model.Movie;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Shane
 */
public class MovieTest extends Movie {

    Movie m;

    @Before
    public void setUp() throws Exception {
        m = new Movie("Star Wars: TFA", "Description goes here", 500.00, 7.50, 120, true);
    }

    @After
    public void tearDown() throws Exception {
        m = null;
    }

    /**
     * Test of getName method, of class m.
     */
    @Test
    public void testgetName() {
        System.out.println("getName");
        String expResult = "Star Wars: TFA";
        String result = m.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDesc method, of class m.
     */
    @Test
    public void testgetDesc() {
        System.out.println("getDesc");
        String expResult = "Description goes here";
        String result = m.getDesc();
        assertEquals(expResult, result);

    }

    /**
     * Test of getBudget method, of class m.
     */
    @Test
    public void testgetBudget() {
        System.out.println("getBudget");
        Double expResult = 500.00;
        Double result = m.getBudget();
        assertEquals(expResult, result);

    }

    /**
     * Test of getTicketPrice method, of class m.
     */
    @Test
    public void testgetTicketPrice() {
        System.out.println("getTicketPrice");
        Double expResult = 7.50;
        Double result = m.getTicketPrice();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRuntime method, of class m.
     */
    @Test
    public void testgetRuntime() {
        System.out.println("getRuntime");
        int expResult = 120;
        int result = m.getRuntime();
        assertEquals(expResult, result);

    }

    /**
     * Test of isIs3D method, of class m.
     */
    @Test
    public void testisIs3D() {
        System.out.println("isIs3D");
        Boolean expResult = true;
        Boolean result = m.isIs3D();
        assertEquals(expResult, result);

    }

    /**
     * Test of HashCode method, of class m.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = -1010844810;
        int result = m.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class m.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        boolean expResult = false;
        boolean result = m.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class m.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "Movies{name=Star Wars: TFA, desc=Description goes here, budget=500.0, ticketPrice=7.5, runtime=120, is3D=true}";
        String result = m.toString();
        assertEquals(expResult, result);
    }
}
